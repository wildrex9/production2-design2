﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite infected;
    public GameObject InfectedParticles;

    private void Start()
    {
       InfectedParticles.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        spriteRenderer.sprite = infected;
    }
    private void HandleParticles()
    {
        if (spriteRenderer.sprite = infected)//if the player infects an NPC activate particly system
        {
            InfectedParticles.SetActive(true);
        }
        if (spriteRenderer.sprite != infected)//if the player infects an NPC activate particly system
        {
            InfectedParticles.SetActive(false);
        }

    }

}
